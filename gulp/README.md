# [Human Shapes Starters](../../../)

## Gulp Workflow

### Setup

Run the following commands in Terminal

1. Install Node Package Manager: ```sudo npm install```
2. Install Bower: ```bower install```
3. Update Bower: ```bower-update```  
*Feel free to remove any resolutions from gulpfile.js.*
4. Search "humanshapes" and "human shape" and replace with your site's information.

### Usage

Install additional bower components:  
```bower install --save```

**Gulp Tasks**

- ```gulp browser-sync```
- ```gulp favicons```
- ```gulp bower```
- ```gulp fonts```
- ```gulp root```
- ```gulp img```
- ```gulp js```
- ```gulp css```
- ```gulp watch```
- ```gulp build```
- ```gulp``` Runs both build and watch
