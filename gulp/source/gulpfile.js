// =============================================
// Dependencies
// =============================================

var gulp = require('gulp'),
    plugin = require('gulp-load-plugins')(),
    gulpif = require('gulp-if'),
    browserSync = require('browser-sync').create();

// =============================================
// SETUP
// =============================================

var projectName = 'HumanShapes',
    localURL = 'humanshapes.dev',
    isProduction = false;

if (plugin.util.env.production === true) {
  isProduction = true;
}

// =============================================
// Paths
// =============================================

var basePath = {
  src: './',
  dist: '../public',
  bowerDir: './bower_components'
};

var path = {
  css: basePath.src + '/css/site.scss',
  scss: basePath.src + '/css/**/*.scss',
  js: [
    basePath.bowerDir + '/jquery/dist/jquery.js',
    basePath.bowerDir + '/respond/dest/respond.src.js',
    basePath.bowerDir + '/jquery-cycle2/build/jquery.cycle2.js',
    basePath.bowerDir + '/jquery-cycle2/src/jquery.cycle2.center.js',
    basePath.bowerDir + '/magnific-popup/dist/jquery.magnific-popup.js',
    basePath.bowerDir + '/sticky-kit/jquery.sticky-kit.js',
    basePath.src + '/js/**/*.coffee'
  ],
  img: [
    basePath.src + '/img/**/*.png',
    basePath.src + '/img/**/*.jpg',
    basePath.src + '/img/**/*.jpeg',
    basePath.src + '/img/**/*.gif',
    basePath.src + '/img/**/*.svg'
  ],
  fonts: [
    basePath.src + '/fonts/**/*.eot',
    basePath.src + '/fonts/**/*.otf',
    basePath.src + '/fonts/**/*.ttf',
    basePath.src + '/fonts/**/*.woff',
    basePath.src + '/fonts/**/*.woff2',
    basePath.src + '/fonts/**/*.svg'
  ],
  favicons: basePath.src + '/favicons/source.png',
  root: basePath.src + '/root/**/*'
}

// =============================================
// Options
// =============================================

var option = {
  autoprefixer: [
    'last 2 version',
    'safari 5',
    'opera 12.1',
    'ios 6',
    'android 4'
  ],
  imageopt: {
    progressive: true,
    svgoPlugins: [{removeViewBox: false}],
    optimizationLevel: 7
  },
  favicons: {
    files: {
      src: path.favicons,
      dest: '../' + basePath.dist + '/favicons',
      html: "../craft/templates/_includes/favicons.html",
      iconsPath: "/favicons/"
    },
    settings: {
      appName: projectName,
      developer: "Human Shapes",
      developerURL: "http://humanshapes.co",
      background: "#FFF"
    }
  }
};

// =============================================
// BROWSER SYNC `gulp browser-sync`
// =============================================

gulp.task('browser-sync', function() {
  browserSync.init(null, {
    proxy: localURL,
    online: false,
    open: true
  });
});

// =============================================
// FAVICONS `gulp favicons`
// =============================================

gulp.task('favicons', function() {
  return gulp.src(path.favicons)
  .pipe(plugin.favicons(option.favicons))
});

// =============================================
// BOWER `gulp bower`
// =============================================

gulp.task('bower', function() {
  return plugin.bower()
    .pipe(gulp.dest(basePath.bowerDir));
});

// =============================================
// FONTS `gulp fonts`
// TODO: Minimize Fonts?
// =============================================

gulp.task('fonts', function() {
  return gulp.src(path.fonts)
  .pipe(gulp.dest(basePath.dist + '/fonts'));
});

// =============================================
// ROOT `gulp root` -- Files for root
// =============================================

gulp.task('root', function() {
  return gulp.src(path.root)
  .pipe(gulp.dest(basePath.dist + '/'));
});

// =============================================
// IMG `gulp img`
// =============================================

gulp.task('img', function() {
  return gulp.src(path.img)
  .pipe(plugin.imagemin(option.imageopt))
  .pipe(gulp.dest(basePath.dist + '/img'));
});

// =============================================
// JS `gulp js`
// =============================================

gulp.task('js', function() {
  return gulp.src(path.js)
  .pipe(plugin.sourcemaps.init())
  .pipe(gulpif(/[.]coffee$/, plugin.coffee({bare: true}).on('error', plugin.util.log)))
  .pipe(plugin.concat('scripts.js'))
  .pipe(gulpif(/[.]coffee$/, plugin.jshint()))
  .pipe(gulpif(/[.]coffee$/, plugin.jshint.reporter('default')))
  .pipe(isProduction ? plugin.uglify() : plugin.util.noop())
  .pipe(plugin.sourcemaps.write('./maps'))
  .pipe(gulp.dest(basePath.dist + '/js'))
  .pipe(browserSync.reload({stream: true}));
});

// =============================================
// CSS `gulp css`
// =============================================

gulp.task('css', function() {
  return gulp.src(path.css)
    .pipe(plugin.sourcemaps.init())
    .pipe(plugin.clipEmptyFiles())
    .pipe(plugin.sass())
    .pipe(isProduction ? plugin.combineMq() : plugin.util.noop())
    .pipe(isProduction ? plugin.minifyCss() : plugin.util.noop())
    .pipe(plugin.sourcemaps.write('./maps'))
    .pipe(gulp.dest(basePath.dist + '/css'))
    .pipe(browserSync.stream({match: '**/*.css'}));
});

// =============================================
// Watch 'gulp watch'
// =============================================

gulp.task('watch',['browser-sync'], function() {
  gulp.watch(path.scss, ['css']);
  gulp.watch(path.js, ['js']);
  gulp.watch(path.img, ['img']);
  gulp.watch(path.fonts, ['fonts']);
  gulp.watch(path.icons, ['icons']);
});

// =============================================
// Build 'gulp build'
// =============================================

gulp.task('build', ['bower', 'css', 'js', 'img', 'fonts', 'root']);

// =============================================
// Default 'gulp'
// =============================================

gulp.task('default', ['build', 'watch']);
