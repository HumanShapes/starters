$(document).on "ready", ->
    HumanShapes.init()

HumanShapes =

  onLoad: ->
    HumanShapes.onResize()

  onResize: ->

  init: ->
    HumanShapes.onLoad()
    $(window).resize ->
        HumanShapes.onResize()

    # Place javascript here

    class HumanShapes.Controller
